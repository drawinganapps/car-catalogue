const { shareAll, withModuleFederationPlugin } = require('@angular-architects/module-federation/webpack');

module.exports = withModuleFederationPlugin({
  name: 'carCatalogue',
  filename: "remoteEntry.js",
  exposes: {
    './CarListModule': './src/app/views/shared/car-list/car-list.module.ts',
    './CarDetailModule': './src/app/views/shared/car-detail/car-detail.module.ts'
  },
  shared: {
    ...shareAll({ singleton: true, strictVersion: false, requiredVersion: 'auto' }),
  },
});
