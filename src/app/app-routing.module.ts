import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

const routes: Routes = [
  {
    path: 'cars/list',
    loadChildren: () => import('./views/car-list-page/car-list-page.module').then(m => m.CarListPageModule)
  },
  {
    path: 'car-details/:id/view',
    loadChildren: () => import('./views/car-detail-page/car-detail-page.module').then(m => m.CarDetailPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
