import {Component, OnInit} from '@angular/core';
import {CarService} from "../../../../core/car/service/car.service";
import {CarModel} from "../../../../core/car/model/car.model";
import {take} from "rxjs";
import {Router} from "@angular/router";

@Component({
  selector: 'app-car-list',
  templateUrl: './car-list.component.html',
  styleUrls: ['./car-list.component.scss']
})
export class CarListComponent implements OnInit {

  cars: CarModel[] = [];

  constructor(private carService: CarService, private router: Router) {
  }

  ngOnInit(): void {
    this.carService.findAllCars().pipe(take(1))
      .subscribe(cars => {
        this.cars = cars;
      });
  }

  searchBike(text: string): void {
    this.carService.findCars(text).pipe(
      take(1)
    ).subscribe(cars => {
      this.cars = cars;
    });
  }

  getImageUrl(image: string): string {
    return `http://localhost:5200/assets/${image}`;
  }

  getLogoUrl(image: string): string {
    return `http://localhost:5200/assets/logo/${image}.png`;
  }

  navigateToBikeDetails(id: number): void {

    console.log(localStorage.getItem('hello'));

    this.router.navigate([`/car-details/${id}/view`]);
  }
}
