import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CarListComponent } from './car-list/car-list.component';
import {SearchModule} from "../search/search.module";
import {CarModule} from "../../../core/car/car.module";
import {RouterModule, Routes} from "@angular/router";

const routes: Routes = [
  {
    path: 'list',
    component: CarListComponent
  }
];

@NgModule({
  declarations: [
    CarListComponent
  ],
  exports: [
    CarListComponent
  ],
  imports: [
    CommonModule,
    SearchModule,
    CarModule,
    RouterModule.forChild(routes)
  ]
})
export class CarListModule { }
