import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderContainerComponent } from './header-container/header-container.component';
import {MatIconModule} from "@angular/material/icon";



@NgModule({
  declarations: [
    HeaderContainerComponent
  ],
  exports: [
    HeaderContainerComponent
  ],
  imports: [
    CommonModule,
    MatIconModule
  ]
})
export class HeaderModule { }
