import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CarDetailComponent} from './car-detail/car-detail.component';
import {MatIconModule} from "@angular/material/icon";
import {RouterModule, Routes} from "@angular/router";
import {CarModule} from "../../../core/car/car.module";

const routes: Routes = [
  {
    path: 'view',
    component: CarDetailComponent
  }
];

@NgModule({
  declarations: [
    CarDetailComponent
  ],
  exports: [
    CarDetailComponent
  ],
  imports: [
    CommonModule,
    MatIconModule,
    RouterModule.forChild(routes),
    CarModule
  ]
})
export class CarDetailModule {
}
