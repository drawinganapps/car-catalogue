import { Component, OnInit } from '@angular/core';
import {CarModel} from "../../../../core/car/model/car.model";
import {DummyData} from "../../../../core/dummy.data";
import {ActivatedRoute, Router} from "@angular/router";
import {CarService} from "../../../../core/car/service/car.service";
import {switchMap, take} from "rxjs";

@Component({
  selector: 'app-car-detail',
  templateUrl: './car-detail.component.html',
  styleUrls: ['./car-detail.component.scss']
})
export class CarDetailComponent implements OnInit {

  car: CarModel;
  readonly DummyData = DummyData;

  constructor(private activatedRoute: ActivatedRoute, private carService: CarService, private router: Router) {
  }

  ngOnInit(): void {
    this.activatedRoute.params.pipe(
      take(1),
      switchMap(params => {
        const carId = params['id'];
        return this.carService.findCarById(carId);
      })
    ).subscribe((car) => {
      this.car = car;
    });
  }

  loadImageUrl(url: string): string {
    return `http://localhost:5200/assets/${url}`;
  }

  getLogoUrl(image: string): string {
    return `http://localhost:5200/assets/logo/${image}.png`;
  }

  onNavigateBack(): void {
    this.router.navigate(['/cars/list']);
  }

}
