import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormControl} from "@angular/forms";

@Component({
  selector: 'app-search-container',
  templateUrl: './search-container.component.html',
  styleUrls: ['./search-container.component.scss']
})
export class SearchContainerComponent implements OnInit {

  @Output() searchText: EventEmitter<string> = new EventEmitter<string>();

  searchInput: FormControl = new FormControl('');


  constructor() { }

  ngOnInit(): void {
  }

  onSearchBike(): void {
    this.searchText.emit(this.searchInput.value);
  }

}
