import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CarListPageComponent} from './car-list-page/car-list-page.component';
import {RouterModule, Routes} from "@angular/router";
import {HeaderModule} from "../shared/header/header.module";
import {CarListModule} from "../shared/car-list/car-list.module";

const routes: Routes = [
  {
    path: '',
    component: CarListPageComponent
  }
];

@NgModule({
  declarations: [
    CarListPageComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    HeaderModule,
    CarListModule
  ]
})
export class CarListPageModule {
}
