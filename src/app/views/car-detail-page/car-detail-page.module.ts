import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CarDetailPageComponent} from './car-detail-page/car-detail-page.component';
import {HeaderModule} from "../shared/header/header.module";
import {CarDetailModule} from "../shared/car-detail/car-detail.module";
import {RouterModule, Routes} from "@angular/router";

const routes: Routes = [
  {
    path: '',
    component: CarDetailPageComponent
  }
];

@NgModule({
  declarations: [
    CarDetailPageComponent
  ],
  imports: [
    CommonModule,
    HeaderModule,
    CarDetailModule,
    RouterModule.forChild(routes)
  ]
})
export class CarDetailPageModule {
}
