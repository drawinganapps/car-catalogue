export enum BrandEnum {
  TOYOTA = 'Toyota',
  HYUNDAI = 'Hyundai',
  HONDA = 'Honda',
  MITSUBISHI = 'Mitsubishi',
  KIA = 'Kia',
  SUZUKI = 'Suzuki',
}
