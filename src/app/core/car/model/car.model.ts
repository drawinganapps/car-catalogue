import {BrandEnum} from "./brand.enum";

export interface CarModel {
  id: number;
  name: string;
  price: number;
  picture: string;
  brand: BrandEnum
  year: number;
}
