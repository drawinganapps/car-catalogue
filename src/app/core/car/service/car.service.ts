import {Injectable} from '@angular/core';
import {Observable, of} from "rxjs";
import {CarModel} from "../model/car.model";
import {DummyData} from "../../dummy.data";

@Injectable()
export class CarService {

  constructor() {
  }

  findAllCars(): Observable<CarModel[]> {
    return of(DummyData.cars);
  }

  findCars(query: string): Observable<CarModel[]> {
    if (!query || query === '') {
      return this.findAllCars();
    }

    const bikes = DummyData.cars.filter(car => {
      return car.name.toLowerCase().includes(query.toLowerCase());
    });
    return of(bikes);
  }

  findCarById(id: string): Observable<CarModel> {
    const findCar: CarModel = DummyData.cars.find(car => {
      return Number(car.id) === Number(id);
    });
    return of(findCar);
  }
}
